#!/usr/bin/python

import application

import tornado.httpserver
import tornado.ioloop
import tornado.options

app = application.Application()

if __name__ == "__main__":
	tornado.options.enable_pretty_logging()
	http_server = tornado.httpserver.HTTPServer(app)
	http_server.listen(8080)
	tornado.ioloop.IOLoop.instance().start()
