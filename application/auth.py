#!/usr/bin/python

from handlers import BaseHandler

class LoginHandler(BaseHandler):
	def get(self):
		self.render("login.html", footer=self.footer())

	def post(self):
		self.set_secure_cookie("user", self.get_argument("user"))
		self.redirect("/")


class LogoutHandler(BaseHandler):
	def get(self):
		self.clear_cookie("user")
		self.render("logout.html", footer=self.footer())
