#!/usr/bin/python

import socket

import tornado.web

class BaseHandler(tornado.web.RequestHandler):
	def get_current_user(self):
		return self.get_secure_cookie("user")

	def footer(self):
		return "%s" % socket.gethostname()

	@property
	def ldap(self):
		return self.application.ldap


class MainHandler(BaseHandler):
	def get(self):
		if not self.current_user:
			self.redirect("/login")
			return
		self.redirect("/home")


class HomeHandler(BaseHandler):
	def get(self):
		self.render("base.html", title="Testsite", slogan="Security now!",
			footer=self.footer(), user=self.current_user)
