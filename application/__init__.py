#!/usr/bin/python

import os
import ldap
import sys
import tornado.web

BASEDIR = os.path.join(os.path.dirname(__file__), "..")

from auth import LoginHandler, LogoutHandler
from handlers import HomeHandler, MainHandler

class Application(tornado.web.Application):
	def __init__(self):
		settings = dict(
			template_path = os.path.join(BASEDIR, "templates"),
			static_path = os.path.join(BASEDIR, "static"),
			xsrf_cookies = True,
			cookie_secret = "123456789",
			login_url = "/login",
			debug = True,
		)

		handlers = [
			(r"/", MainHandler),
			(r"/home", HomeHandler),
			(r"/login", LoginHandler),
			(r"/logout", LogoutHandler),
		] #self.get_handlers()

		self.ldap = None # ldap.initialize("ldap://...")

		tornado.web.Application.__init__(self, handlers, **settings)

#	# XXX This is not a nice solution but works for the moment
#	def get_handlers(self):
#		handlers_dir = os.path.join(BASEDIR, "pages")
#		sys.path.append(handlers_dir)
#		handlers = []
#
#		for handler in os.listdir(handlers_dir):
#			if not handler.endswith(".py"):
#				continue
#
#			handler = handler[:-3]
#			handler = __import__(handler)
#			
#			if type(handler.handle) == type([]):
#				handlers.extend(handler.handle)
#			else:
#				handlers.append(handler.handle)
#
#		return handlers
